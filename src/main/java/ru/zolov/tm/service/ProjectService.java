package ru.zolov.tm.service;

import ru.zolov.tm.entity.Project;
import ru.zolov.tm.repository.ProjectRepository;
import ru.zolov.tm.repository.TaskRepository;

import java.util.Map;

public class ProjectService {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void create(String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception("Id input empty");
        projectRepository.persist(name);
    }

    public Project read(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception("Empty input");
        return projectRepository.find(id);
    }

    public Map<String, Project> readAll() throws Exception {
        if (projectRepository.findAll() == null || projectRepository.findAll().isEmpty())
            throw new Exception("Empty storage");
        return projectRepository.findAll();
    }

    public void update(String id, String name) {
        if (id == null || id.isEmpty()) return;
        for (Project p : projectRepository.findAll().values()) {
            if (id.equals(p.getId())) {
                p.setName(name);
            }
        }
    }

    public boolean remove(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        taskRepository.removeAllByProjectID(id);
        return projectRepository.remove(id);
    }
}
