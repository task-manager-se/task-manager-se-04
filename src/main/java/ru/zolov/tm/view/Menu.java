package ru.zolov.tm.view;

import ru.zolov.tm.entity.Project;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.TaskService;

import java.util.Map;
import java.util.Scanner;

public class Menu {
    protected String inp = "Input: ";
    private Scanner scanner;
    private ProjectService projects;
    private TaskService tasks;
    private Project currentProject;


    public Menu(Scanner scanner, ProjectService projects, TaskService tasks) {
        this.scanner = scanner;
        this.projects = projects;
        this.tasks = tasks;
    }


    public void drawHeader() {
        System.out.println("+-------------------------------+");
        System.out.println("|  Welcome to Task manager 0.1  |");
        System.out.println("+-------------------------------+");
        System.out.println("Enter help for display list of commands");
        System.out.print(inp);
    }

    public void drawHelp() {
        System.out.println("Enter cp for create project");
        System.out.println("Enter dp for display list of project");
        System.out.println("Enter ep for edit project");
        System.out.println("Enter rp for remove project");
        System.out.println("Enter ct for create task");
        System.out.println("Enter dt for display tasks");
        System.out.println("Enter et for edit task");
        System.out.println("Enter rt for remove some task");
        System.out.print(inp);
    }

    public void createProject() throws Exception {
        String input;
        System.out.print("Enter project name:");
        input = scanner.nextLine();
        projects.create(input);
        System.out.println("[OK]");
        System.out.print(inp);
    }

    public void displayProjects() throws Exception {
        output(projects.readAll());
        System.out.println(inp);
    }

    public void editProject() {
        System.out.println("Enter id:");
        String inputId = scanner.nextLine();
        System.out.println("Enter project new name");
        String name = scanner.nextLine();
        projects.update(inputId, name);
        System.out.print(inp);
    }

    public void removeProject() throws Exception {
        output(projects.readAll());
        System.out.println("Enter id:");
        String input = scanner.nextLine();
        System.out.println(projects.remove(input));

        System.out.print(inp);
    }

    public void createTask() throws Exception {
        output(projects.readAll());
        System.out.print("Enter project id: ");
        String projectId = scanner.nextLine();

        tasks.create(projectId);
        tasks.readTaskByProjectId(projectId);
        System.out.print(inp);
    }

    public void removeTask() throws Exception {
        tasks.readAll();
        System.out.print("Enter task id: ");
        String input = scanner.nextLine();
        tasks.remove(input);
        System.out.println("DONE!");
        System.out.print(inp);
    }

    public void editTask() throws Exception {
        output(tasks.readAll());
        System.out.print("Enter task id: ");
        String input = scanner.nextLine();
        System.out.println("Enter new description: ");
        String description = scanner.nextLine();
        tasks.update(input, description);
        System.out.println("DONE!");
        tasks.readTaskById(input);
        System.out.print(inp);
    }

    public void displayTasks() throws Exception {
        System.out.print("Enter project id: ");
        String input = scanner.nextLine();
        output(tasks.readTaskByProjectId(input));
        System.out.print(inp);
    }

    public void output(Map<String, ?> objects) {
        for (Object object : objects.values()) {
            System.out.println(object);
        }
    }
}
