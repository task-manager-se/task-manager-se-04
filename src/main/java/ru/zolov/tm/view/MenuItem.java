package ru.zolov.tm.view;

public enum MenuItem {

    HELP("help"),
    CREATE_PROJECT("cp"),
    DISPLAY_PROJECTS("dp"),
    EDIT_PROJECT("ep"),
    REMOVE_PROJECT("rp"),
    CREATE_TASK("ct"),
    DISPLAY_TASKS("dt"),
    EDIT_TASK("et"),
    REMOVE_TASK("rt");

    private final String name;

    MenuItem(String name) {
        this.name = name;
    }

    public static MenuItem checkName(String name) {
        MenuItem[] items = MenuItem.values();
        for (MenuItem i : items) {
            if (name.equals(i.getName())) return i;
        }
        return null;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
