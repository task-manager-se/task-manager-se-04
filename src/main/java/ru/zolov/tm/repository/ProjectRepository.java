package ru.zolov.tm.repository;

import ru.zolov.tm.entity.Project;

import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {
    private Map<String, Project> projects = new LinkedHashMap<>();

    public ProjectRepository() {
    }

    public void persist(String name) {
        Project project = new Project(name);
        projects.put(project.getId(), project);
    }

    public Project find(String id) {
        return projects.get(id);
    }

    public Map<String, Project> findAll() {
        return projects;
    }

    public void update(String id, String name) {
        Project project = projects.get(id);
        project.setName(name);
    }

    public boolean remove(String id) {
        return projects.remove(id) != null;
    }

    public void removeAll() {
        projects.clear();
    }

    public void merge(String id, String name) {
        if (find(id) == null) {
            persist(name);
        } else {
            update(id, name);
        }
    }

}
