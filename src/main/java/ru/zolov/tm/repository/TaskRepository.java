package ru.zolov.tm.repository;

import ru.zolov.tm.entity.Task;

import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {
    private Map<String, Task> tasks = new LinkedHashMap<>();

    public void persist(String id) {
        Task task = new Task(id);
        tasks.put(task.getId(), task);

    }

    public void persist(String id, String name) {
        Task task = new Task(id);
        task.setName(name);
        tasks.put(task.getId(), task);
    }

    public Task find(String id) {
        return tasks.get(id);
    }

    public Map<String, Task> findAll() {
        return tasks;
    }

    public Map<String, Task> findTaskByProjId(String id) {
        Map<String, Task> result = new LinkedHashMap<>();

        for (Task task : tasks.values()) {
            if (task.getProjectId().equals(id)) {
                result.put(task.getId(), task);
            }
        }
        return result;
    }

    public void update(String id, String description) {
        Task task = tasks.get(id);
        task.setName(description);
    }

    public boolean remove(String id) {
        return tasks.remove(id) != null;
    }

    public void removeAll() {
        tasks.clear();
    }

    public boolean removeAllByProjectID(String id) {
        for (Task task : tasks.values()) {
            if (task.getProjectId().equals(id)) {
                tasks.remove(task.getId());
                return true;
            }
        }
        return false;
    }

    public void merge(String id, String name) {
        if (find(id) == null) {
            persist(name);
        } else {
            update(id, name);
        }
    }
}


